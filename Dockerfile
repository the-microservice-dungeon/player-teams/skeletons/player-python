FROM python:3.12.1-alpine3.19

WORKDIR /app

# Installieren von Abhängigkeiten
COPY ./requirements.txt ./requirements.txt
RUN pip install -r requirements.txt

# Kopieren der gesamten src-Struktur
COPY ./src ./src

# Setzen des PYTHONPATH
ENV PYTHONPATH /app

# Ausführen der Anwendung
CMD ["python", "src/main/DungeonPlayerMainApplication.py"]