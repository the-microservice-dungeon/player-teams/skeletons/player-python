# Table of Contents

- [Player Skeleton Python](#player-skeleton-python)
- [Requirements:](#requirements)
- [Preparation](#preparation)
- [Running the Player](#running-the-player)
    - [Configuration](#configuration)
    - [Tests](#tests)
- [Event Listening](#event-listening)
- [Data Endpoint](#data-endpoint)
- [Dev Mode](#dev-mode)
- [How to Continue from Here](#how-to-continue-from-here)
- [Authors](#authors)

## Player Skeleton Python

This is the Documentation of the Player Skeleton for the microservice dungeon, which is written in Python.
You can use this player as basis for your own player.
It already implemented the basic functionality of a player:

* Creating(Dev Mode), Registering, Joining, Starting, Ending and Persisting Games
* Listening and Logging for incoming Events
* Parsing basic Events into Event Classes
* Handle Incoming events by calling the Handler of the specific event class (e.g. GameStatusEventHandler)
* Domain Primitives you can use to build your player
* Tests for the basic functionality

## Requirements:

- Python 3.12 with pip installed
- Local Dev Environment

## Preparation

To use [this skeleton](https://gitlab.com/the-microservice-dungeon/player-teams/skeletons/player-python) as the base for
your player development, you need to accomplish the following steps.

First, fork this repository and create a new repository under
the [Player Teams subgroup](https://gitlab.com/the-microservice-dungeon/player-teams) (or in any other Git location).
The fork should be named after your desired player name, for example `player-constantine`.

Now you need to add your player-name to a few files. The required places are marked using TODO comments.
Update the files in `helm-chart/Chart.yaml` `helm-chart/values.yaml` and `.gitlab-ci.yml`.

## Running the Player

Install the dependencies using pip:

```bash
  pip install -r requirements.txt
```

You can then simply run the player by executing the following command (or run it from your IDE):

```bash
  python src/main/DungeonPlayerMainApplication.py
```

### Configuration

The player can be configured using environment variables:

| Environment Variable | Default                            |
|----------------------|------------------------------------|
| DEV                  | False                              |
| RABBITMQ_HOST        | 127.0.0.1                          |
| RABBITMQ_PORT        | 5672                               |
| RABBITMQ_USERNAME    | admin                              |
| RABBITMQ_PASSWORD    | admin                              |
| GAME_HOST            | http://127.0.0.1                   |
| GAME_PORT            | 8080                               |
| PLAYER_NAME          | player-skeleton-python             |
| PLAYER_EMAIL         | player-skeleton-python@example.com |
| LOG_LEVEL            | INFO                               |
| DATA_ENDPOINT_PORT:  | 8090                               |

### Tests

The player skeleton includes several tests that you can enhance for your implementation:

- **Running tests via command line**:

```bash
  python -m unittest discover -s src/test -v
```

- **Running tests via IDE**: Tests can be run from your IDE (e.g. PyCharm) as well.

## Event Listening

The skeleton player utilizes a single messaging queue for all events. It listens on the player-owned queue for events,
deserializes them, and dispatches them to the appropriate event handler
via `src.main.core.events.eventlistener.EventDispatcher.py`. Currently, two event handlers are implemented:

- For `GameStatus` events: `src.main.game.application.GameStatusEventHandler.py`
- For `RoundStatus` events: `src.main.game.application.RoundStatusEventHandler.py`

## Data Endpoint

In case you want to provide an API to access your player data e.g. for a map frontend or a statistics backend, you can
do so by implementing a data endpoint. The data endpoint is a simple HTTP server that provides a REST API to access your
player data. The port of the data endpoint can be configured via the `DATA_ENDPOINT_PORT` environment variable.

## Dev Mode

**Dev mode** is available for local development and can be enabled through an environment variable. It automates the
game creation and start process:

- **To enable**: Set the `DEV` environment variable to `True`.
- **Note**: This feature is intended **only for local development**.

## How to Continue from Here

With the event listening and handling framework in place, the next steps involve:

1. **Set up a Running Local Dev Environmnent**: Set up
   a [Local dev environment](https://gitlab.com/the-microservice-dungeon/devops-team/local-dev-environment) to run &
   test your player in games
   locally.
2. **Understanding Game Events**: Familiarize yourself with the different game events. Consider the information each
   event provides and how it can be used for decision making in the game.
3. **Add Missing Event Classes**: In case you encounter any missing events you can add them in
   the `src.main.core.events.concreteevents` package. These classes are
   required to deserialize the events received from the game.
4. **Implementing Event Handlers**: Develop your own Event Handlers to build your own "view" of the game state. This
   view can be used to make decisions in the game.
5. **Send Commands** : Once you have a view of the game state you can start to implement sending commands based on the
   game state. You should probably send commands or your Player does not do anything :D.
6. **Deployments**: Once you are happy with the state of your player it will be time to get the Deployment for it
   running.
7. **Have fun and be creative**: There are many ways to play the game. Be creative and have fun!

## Deployments (Near end of project)

Deployments are probably the last thing you have to do before the project is finished.
Make sure to adjust the `values.yaml` in the `helm-chart` folder to the projects needs!
Speak with the DevOps team if you run into issues.

## Further Reading

- [Learning About Asynchronous Communication](https://the-microservice-dungeon.gitlab.io/docs/docs/reference/asynchronous-communication/)
- [Pydantic](https://docs.pydantic.dev/latest/): Pydantic is used for parsing the incoming events into event classes.
- [Python](https://docs.python.org/3/): Documentation for the Python Programming Language.

## Authors

- [Andre Müller](https://gitlab.com/Amueller36)
