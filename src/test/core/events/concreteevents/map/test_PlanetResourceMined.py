from unittest import TestCase

from src.main.core.events.concreteevents.map.PlanetResourceMined import PlanetResourceMinedPayload


class TestPlanetResourceMinedPayload(TestCase):
    def test_parseFromJson(self):
        json = {
            "planet": "12345",
            "resource": {
                "resourceType": "IRON",
                "maxAmount": 100,
                "currentAmount": 95
            },
            "minedAmount": 5
        }
        #try to parse into PlanetResourceMinedPayload

        payload = PlanetResourceMinedPayload(**json)
        self.assertEqual("12345", payload.planetId)
        self.assertEqual(5, payload.minedAmount)
        self.assertEqual("IRON", payload.resource.resourceType.value)
        self.assertEqual(100, payload.resource.maxAmount)
        self.assertEqual(95, payload.resource.currentAmount)
