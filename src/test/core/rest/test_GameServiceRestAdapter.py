import unittest
from http import HTTPStatus
from unittest.mock import patch

from src.main.core.restadapter.GameServiceRestAdapter import GameServiceRestAdapter
from src.main.core.restadapter.RestAdapterException import RestAdapterException
from src.main.core.restadapter.res.GameInfoResponseBody import GameInfoResponseBody


class TestGameServiceRestAdapter(unittest.TestCase):
    game_info_response_mock = GameInfoResponseBody(gameId='1', gameStatus='WAITING_FOR_PLAYERS', maxPlayers=4,
                                                   maxRounds=10, roundLengthInMillis=10000, currentRoundNumber=1,
                                                   participatingPlayers=[])

    @patch('requests.get')
    def test_getGames_success(self, mock_get):
        # Erfolgsfall
        mock_get.return_value.status_code = HTTPStatus.OK
        mock_get.return_value.json.return_value = [self.game_info_response_mock.model_dump()]

        games = GameServiceRestAdapter.getGames()
        self.assertIsNotNone(games)
        self.assertEqual(len(games), 1)
        self.assertEqual(games[0].gameId, '1')

    @patch('requests.get')
    def test_getGames_failure(self, mock_get):
        # Fehlerfall
        mock_get.return_value.status_code = HTTPStatus.OK
        mock_get.return_value.json.return_value = []

        games = GameServiceRestAdapter.getGames()
        self.assertIsNotNone(games)
        self.assertEqual(len(games), 0)

    @patch('requests.post')
    def test_registerPlayer_success(self, mock_post):
        # Erfolgsfall
        mock_post.return_value.status_code = HTTPStatus.CREATED
        mock_post.return_value.json.return_value = {'playerId': '1', 'name': 'Test', 'email': 'test@example.com'}

        player = GameServiceRestAdapter.registerPlayer(name='Test', email="test@example.com")
        self.assertIsNotNone(player)
        self.assertEqual(player.playerId, '1')

    @patch('requests.post')
    def test_player_registration_failed_due_to_duplicate_name(self, mock_post):
        mock_post.return_value.status_code = HTTPStatus.BAD_REQUEST

        result = GameServiceRestAdapter.registerPlayer(name='Test', email="test@example.com")

        self.assertIsNone(result)

    @patch('requests.post')
    def test_player_registration_failed_due_to_unknown_error(self, mock_post):
        mock_post.return_value.status_code = HTTPStatus.INTERNAL_SERVER_ERROR

        with self.assertRaises(RestAdapterException):
            GameServiceRestAdapter.registerPlayer(name='Test', email="test@example.com")

    @patch('requests.post')
    def test_createGame_success(self, mock_post):
        # Erfolgsfall
        mock_post.return_value.status_code = HTTPStatus.CREATED
        mock_post.return_value.json.return_value = {'gameId': '1'}

        game = GameServiceRestAdapter.createGame(maxPlayers=4, maxRounds=10)
        self.assertIsNotNone(game)
        self.assertEqual(game.gameId, '1')

    @patch('requests.post')
    def test_createGame_failure(self, mock_post):
        # Fehlerfall
        mock_post.return_value.status_code = HTTPStatus.BAD_REQUEST
        mock_post.return_value.json.return_value = {'message': 'Bad request'}

        game = GameServiceRestAdapter.createGame(maxPlayers=4, maxRounds=10)
        self.assertIsNone(game)

    @patch('requests.put')
    def test_joinGame_success(self, mock_put):
        # Erfolgsfall
        mock_put.return_value.status_code = HTTPStatus.OK

        result = GameServiceRestAdapter.joinGame(gameId='1', playerId='1')
        self.assertTrue(result)

    @patch('requests.put')
    def test_joinGame_failure(self, mock_put):
        # Fehlerfall
        mock_put.return_value.status_code = HTTPStatus.BAD_REQUEST

        result = GameServiceRestAdapter.joinGame(gameId='1', playerId='1')
        self.assertFalse(result)

    @patch('requests.put')
    def test_joinGame_failure_due_to_not_found(self, mock_put):
        # Fehlerfall
        mock_put.return_value.status_code = HTTPStatus.NOT_FOUND

        result = GameServiceRestAdapter.joinGame(gameId='1', playerId='1')
        self.assertFalse(result)

    @patch('requests.put')
    def test_joinGame_failure_due_to_unknown_error(self, mock_put):
        # Fehlerfall
        mock_put.return_value.status_code = HTTPStatus.INTERNAL_SERVER_ERROR

        with self.assertRaises(RestAdapterException):
            GameServiceRestAdapter.joinGame(gameId='1', playerId='1')

    @patch('requests.get')
    def test_getPlayer_success(self, mock_get):
        # Erfolgsfall
        mock_get.return_value.status_code = HTTPStatus.OK
        mock_get.return_value.json.return_value = {'playerId': '1', 'name': 'Test', 'email': 'test@example.com'}

        player = GameServiceRestAdapter.getPlayer()
        self.assertIsNotNone(player)
        self.assertEqual(player.playerId, '1')

    @patch('requests.get')
    def test_getPlayer_failure_due_to_non_existent_player(self, mock_get):
        # Fehlerfall
        mock_get.return_value.status_code = HTTPStatus.NOT_FOUND

        player = GameServiceRestAdapter.getPlayer()
        self.assertIsNone(player)

    @patch('requests.get')
    def test_getPlayer_failure_due_to_unknown_error(self, mock_get):
        # Fehlerfall
        mock_get.return_value.status_code = HTTPStatus.INTERNAL_SERVER_ERROR

        with self.assertRaises(RestAdapterException):
            GameServiceRestAdapter.getPlayer()

    @patch('requests.get')
    def test_getPlayer_failure_due_to_bad_request(self, mock_get):
        # Fehlerfall
        mock_get.return_value.status_code = HTTPStatus.BAD_REQUEST

        with self.assertRaises(RestAdapterException):
            GameServiceRestAdapter.getPlayer()
