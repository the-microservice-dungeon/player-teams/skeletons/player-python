from unittest import TestCase

from src.main.core.domainprimitives.DomainPrimitiveException import DomainPrimitiveException
from src.main.core.domainprimitives.purchasing.Money import Money


class TestMoney(TestCase):
    def test_from_amount(self):
        money_5 = Money.fromAmount(5)
        self.assertEqual(5, money_5.amount)
        with self.assertRaises(DomainPrimitiveException):
            Money.fromAmount(None)
        with self.assertRaises(DomainPrimitiveException):
            Money.fromAmount(-1)

    def test_zero(self):
        self.assertEqual(0, Money.zero().amount)

    def test_can_buy_that_many_for(self):
        money_5 = Money.fromAmount(5)
        money_10 = Money.fromAmount(10)
        self.assertEqual(2, money_10.canBuyThatManyFor(money_5))
        with self.assertRaises(DomainPrimitiveException):
            money_10.canBuyThatManyFor(None)

    def test_decrease_by(self):
        money_2 = Money.fromAmount(2)
        money_3 = Money.fromAmount(3)
        self.assertEqual(1, money_3.decreaseBy(money_2).amount)
        with self.assertRaises(DomainPrimitiveException):
            money_2.decreaseBy(None)
        with self.assertRaises(DomainPrimitiveException):
            money_2.decreaseBy(money_3)

    def test_increase_by(self):
        money_2 = Money.fromAmount(2)
        money_3 = Money.fromAmount(3)
        self.assertEqual(5, money_2.increaseBy(money_3).amount)
        with self.assertRaises(DomainPrimitiveException):
            money_2.increaseBy(None)

    def test_greater_than(self):
        money_2 = Money.fromAmount(2)
        money_3 = Money.fromAmount(3)
        self.assertTrue(money_3.greaterThan(money_2))
        with self.assertRaises(DomainPrimitiveException):
            money_2.greaterThan(None)

    def test_greater_equal_than(self):
        money_2 = Money.fromAmount(2)
        money_3 = Money.fromAmount(3)
        self.assertTrue(money_3.greaterEqualThan(money_2))
        self.assertTrue(money_3.greaterEqualThan(money_3))
        with self.assertRaises(DomainPrimitiveException):
            money_2.greaterEqualThan(None)

    def test_equals_and_unequals(self):
        money_2 = Money.fromAmount(2)
        money_3 = Money.fromAmount(3)
        self.assertEqual(money_2, money_2)
        self.assertNotEqual(money_2, money_3)
        self.assertNotEqual(money_2, None)
        self.assertNotEqual(money_2, "Test")
        self.assertEqual(money_2, Money.fromAmount(2))
        self.assertNotEqual(money_2, Money.fromAmount(3))
