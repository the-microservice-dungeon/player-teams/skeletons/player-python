from unittest import TestCase

from src.main.core.domainprimitives.purchasing.ItemType import ItemType


class TestItemType(TestCase):

    def test_string_for_command(self):
        for item_type in ItemType:
            match item_type:
                case ItemType.HEALTH_RESTORE:
                    self.assertEqual(item_type.value, "HEALTH_RESTORE")
                case ItemType.ENERGY_RESTORE:
                    self.assertEqual(item_type.value, "ENERGY_RESTORE")
                case ItemType.ROBOT:
                    self.assertEqual(item_type.value, "ROBOT")
