from unittest import TestCase

from src.main.core.domainprimitives.DomainPrimitiveException import DomainPrimitiveException
from src.main.core.domainprimitives.purchasing.Capability import Capability
from src.main.core.domainprimitives.purchasing.CapabilityType import CapabilityType


class TestCapability(TestCase):
    def test_base_for_type(self):
        for capability_type in CapabilityType:
            capability = Capability.baseForType(capability_type)
            self.assertEqual(capability.capabilityType, capability_type)
            self.assertEqual(Capability.MIN_LEVEL, capability.level)

    def test_forTypeAndLevel_validInputs(self):
        capability = Capability.forTypeAndLevel(CapabilityType.DAMAGE, 3)
        self.assertEqual(capability.capabilityType, CapabilityType.DAMAGE)
        self.assertEqual(capability.level, 3)

    def test_forTypeAndLevel_invalidType(self):
        with self.assertRaises(DomainPrimitiveException):
            Capability.forTypeAndLevel(None, 1)

    def test_forTypeAndLevel_invalidLevel(self):
        with self.assertRaises(DomainPrimitiveException):
            Capability.forTypeAndLevel(CapabilityType.HEALTH, -1)

    def test_allBaseCapabilities(self):
        all_capabilities = Capability.allBaseCapabilities()
        self.assertEqual(len(all_capabilities), len(CapabilityType))
        for capability in all_capabilities:
            self.assertEqual(capability.level, Capability.MIN_LEVEL)

    def test_nextLevel(self):
        capability = Capability.forTypeAndLevel(CapabilityType.ENERGY_REGEN, 3)
        next_level_capability = capability.nextLevel()
        self.assertEqual(next_level_capability.level, 4)

        max_level_capability = Capability.forTypeAndLevel(CapabilityType.MINING, Capability.MAX_LEVEL)
        self.assertIsNone(max_level_capability.nextLevel())

    def test_isMinimumLevel_isMaximumLevel(self):
        min_level_capability = Capability.baseForType(CapabilityType.STORAGE)
        self.assertTrue(min_level_capability.isMinimumLevel())
        self.assertFalse(min_level_capability.isMaximumLevel())

        max_level_capability = Capability.forTypeAndLevel(CapabilityType.MINING_SPEED, Capability.MAX_LEVEL)
        self.assertTrue(max_level_capability.isMaximumLevel())
        self.assertFalse(max_level_capability.isMinimumLevel())

    def test_toStringForCommand(self):
        capability = Capability.forTypeAndLevel(CapabilityType.HEALTH, 2)
        self.assertEqual(capability.toStringForCommand(), "HEALTH_2")
        min_capability = Capability.baseForType(CapabilityType.STORAGE)
        self.assertEqual(min_capability.toStringForCommand(), "STORAGE_0")
