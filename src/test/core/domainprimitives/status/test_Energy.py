from unittest import TestCase

from src.main.core.domainprimitives.DomainPrimitiveException import DomainPrimitiveException
from src.main.core.domainprimitives.status.Energy import Energy


class TestEnergy(TestCase):
    e_1 = Energy.fromAmount(1)

    def test_from_amount(self):
        e_5 = Energy.fromAmount(5)
        self.assertEqual(5, e_5.energyAmount)
        with self.assertRaises(DomainPrimitiveException):
            Energy.fromAmount(None)
        with self.assertRaises(DomainPrimitiveException):
            Energy.fromAmount(-1)

    def test_lower_percentage_than(self):
        e_5 = Energy.fromAmount(5)
        e_10 = Energy.fromAmount(10)
        self.assertTrue(e_5.lowerPercentageThan(50, e_10))
        with self.assertRaises(DomainPrimitiveException):
            e_5.lowerPercentageThan(150, e_10)
        with self.assertRaises(DomainPrimitiveException):
            e_5.lowerPercentageThan(-1, e_10)
        with self.assertRaises(DomainPrimitiveException):
            e_5.lowerPercentageThan(101, e_10)

    def test_zero(self):
        self.assertEqual(0, self.e_1.zero().energyAmount)

    def test_increase_by(self):
        e_2 = Energy.fromAmount(2)
        e_3 = Energy.fromAmount(3)
        self.assertEqual(5, e_2.increaseBy(e_3).energyAmount)
        with self.assertRaises(DomainPrimitiveException):
            e_2.increaseBy(None)

    def test_decrease_by(self):
        e_2 = Energy.fromAmount(2)
        e_3 = Energy.fromAmount(3)
        self.assertEqual(1, e_3.decreaseBy(e_2).energyAmount)
        with self.assertRaises(DomainPrimitiveException):
            e_2.decreaseBy(None)
        with self.assertRaises(DomainPrimitiveException):
            e_2.decreaseBy(e_3)

    def test_greater_than(self):
        e_2 = Energy.fromAmount(2)
        e_3 = Energy.fromAmount(3)
        self.assertTrue(e_3.greaterThan(e_2))
        with self.assertRaises(DomainPrimitiveException):
            e_2.greaterThan(None)

    def test_greater_than_or_equal(self):
        e_2 = Energy.fromAmount(2)
        e_3 = Energy.fromAmount(3)
        self.assertTrue(e_3.greaterThanOrEqual(e_2))
        self.assertTrue(e_3.greaterThanOrEqual(e_3))
        with self.assertRaises(DomainPrimitiveException):
            e_2.greaterThanOrEqual(None)
