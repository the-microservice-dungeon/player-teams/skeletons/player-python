from unittest import TestCase

from src.main.core.domainprimitives.location.CompassDirection import CompassDirection


class TestCompassDirection(TestCase):
    ex_north = CompassDirection.NORTH
    ex_east = CompassDirection.EAST
    ex_south = CompassDirection.SOUTH
    ex_west = CompassDirection.WEST

    def test_get_opposite_direction(self):
        self.assertEqual(self.ex_south, self.ex_north.getOppositeDirection())
        self.assertEqual(self.ex_west, self.ex_east.getOppositeDirection())
        self.assertEqual(self.ex_north, self.ex_south.getOppositeDirection())
        self.assertEqual(self.ex_east, self.ex_west.getOppositeDirection())

    def test_x_offset(self):
        self.assertEqual(0, self.ex_north.xOffset())
        self.assertEqual(1, self.ex_east.xOffset())
        self.assertEqual(0, self.ex_south.xOffset())
        self.assertEqual(-1, self.ex_west.xOffset())

    def test_y_offset(self):
        self.assertEqual(-1, self.ex_north.yOffset())
        self.assertEqual(0, self.ex_east.yOffset())
        self.assertEqual(1, self.ex_south.yOffset())
        self.assertEqual(0, self.ex_west.yOffset())

    def test_ninety_degrees(self):
        self.assertEqual([self.ex_east, self.ex_west], self.ex_north.ninetyDegrees())
        self.assertEqual([self.ex_north, self.ex_south], self.ex_east.ninetyDegrees())
        self.assertEqual([self.ex_east, self.ex_west], self.ex_south.ninetyDegrees())
        self.assertEqual([self.ex_north, self.ex_south], self.ex_west.ninetyDegrees())

    def test_random(self):
        self.assertIn(CompassDirection.random(), list(CompassDirection))
