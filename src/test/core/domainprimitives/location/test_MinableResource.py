from unittest import TestCase

from src.main.core.domainprimitives.DomainPrimitiveException import DomainPrimitiveException
from src.main.core.domainprimitives.location.MinableResource import MinableResource
from src.main.core.domainprimitives.location.MinableResourceType import MinableResourceType


class TestMinableResource(TestCase):
    coal = MinableResource.fromTypeAndAmount(MinableResourceType.COAL, 5)
    iron = MinableResource.fromTypeAndAmount(MinableResourceType.IRON, 5)
    gem = MinableResource.fromTypeAndAmount(MinableResourceType.GEM, 5)
    gold = MinableResource.fromTypeAndAmount(MinableResourceType.GOLD, 5)
    platin = MinableResource.fromTypeAndAmount(MinableResourceType.PLATIN, 5)

    def test_from_type_and_amount(self):
        self.assertEqual(MinableResourceType.COAL, self.coal.type)
        self.assertEqual(5, self.coal.amount)
        with self.assertRaises(DomainPrimitiveException):
            MinableResource.fromTypeAndAmount(None, 5)
        with self.assertRaises(DomainPrimitiveException):
            MinableResource.fromTypeAndAmount(MinableResourceType.COAL, None)
        with self.assertRaises(DomainPrimitiveException):
            MinableResource.fromTypeAndAmount(MinableResourceType.COAL, 0)
        with self.assertRaises(DomainPrimitiveException):
            MinableResource.fromTypeAndAmount(MinableResourceType.COAL, -1)
        self.assertEqual(MinableResourceType.PLATIN, self.platin.type)
        self.assertEqual(5, self.platin.amount)

    def test_add(self):
        coal_2 = MinableResource.fromTypeAndAmount(MinableResourceType.COAL, 2)
        coal_7 = self.coal.add(coal_2)
        self.assertEqual(MinableResourceType.COAL, coal_7.type)
        self.assertEqual(7, coal_7.amount)
        with self.assertRaises(DomainPrimitiveException):
            self.coal.add(None)
        with self.assertRaises(DomainPrimitiveException):
            self.coal.add(MinableResource.fromTypeAndAmount(MinableResourceType.IRON, 2))

    def test_equal_and_unequal(self):
        self.assertEqual(self.coal, self.coal)
        self.assertNotEqual(self.coal, self.iron)
        self.assertNotEqual(self.coal, self.gem)
        self.assertNotEqual(self.coal, self.gold)
        self.assertNotEqual(self.coal, self.platin)
        self.assertNotEqual(self.coal, None)
        self.assertNotEqual(self.coal, "Test")
        self.assertEqual(self.coal, MinableResource.fromTypeAndAmount(MinableResourceType.COAL, 5))
        self.assertNotEqual(self.coal, MinableResource.fromTypeAndAmount(MinableResourceType.COAL, 4))
