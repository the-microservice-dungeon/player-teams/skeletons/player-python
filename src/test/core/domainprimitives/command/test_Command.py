import json
from unittest import TestCase

from src.main.core.domainprimitives.command.Command import Command
from src.main.core.domainprimitives.command.CommandType import CommandType
from src.main.core.domainprimitives.purchasing.Capability import Capability
from src.main.core.domainprimitives.purchasing.CapabilityType import CapabilityType


class TestCommand(TestCase):
    playerId = "12345678-1234-1234-1234-1234567890ab"
    robotId = "32145678-1234-1234-1234-1234567890ac"
    planetId = "21345678-1234-1234-1234-1234567890ad"
    targetRobotId = "41245678-1234-1234-1234-1234567890ae"

    def test_createRobotMoveCommand(self):
        command = Command.createRobotMoveCommand(self.playerId, self.robotId, self.planetId)
        self.assertEqual(command.type, CommandType.MOVEMENT)
        self.assertEqual(str(command.playerId), self.playerId)
        self.assertEqual(str(command.data.robotId), self.robotId)
        self.assertEqual(str(command.data.planetId), self.planetId)

    def test_create_buy_robot_health_restore_command(self):
        command = Command.createBuyRobotHealthRestoreCommand(self.playerId, self.robotId)
        self.assertEqual(command.type, CommandType.BUYING)
        self.assertEqual(str(command.playerId), self.playerId)
        self.assertEqual(str(command.data.robotId), self.robotId)
        self.assertEqual(command.data.itemName, "HEALTH_RESTORE")
        self.assertEqual(command.data.itemQuantity, 1)

    def test_create_buy_robot_energy_restore_command(self):
        command = Command.createBuyRobotEnergyRestoreCommand(self.playerId, self.robotId)
        self.assertEqual(command.type, CommandType.BUYING)
        self.assertEqual(str(command.playerId), self.playerId)
        self.assertEqual(str(command.data.robotId), self.robotId)
        self.assertEqual(command.data.itemName, "ENERGY_RESTORE")
        self.assertEqual(command.data.itemQuantity, 1)

    def test_create_robot_buy_command(self):
        command = Command.createRobotBuyCommand(self.playerId, 5)
        self.assertEqual(command.type, CommandType.BUYING)
        self.assertEqual(str(command.playerId), self.playerId)
        self.assertEqual(command.data.itemName, "ROBOT")
        self.assertEqual(command.data.itemQuantity, 5)

    def test_create_sell_inventory_command(self):
        command = Command.createSellInventoryCommand(self.playerId, self.robotId)
        self.assertEqual(command.type, CommandType.SELLING)
        self.assertEqual(str(command.playerId), self.playerId)
        self.assertEqual(str(command.data.robotId), self.robotId)
        self.assertIsNone(command.data.itemQuantity)
        self.assertIsNone(command.data.itemName)

    def test_create_robot_upgrade_command(self):
        health_upgrade_command = Command.createRobotUpgradeCommand(self.playerId, self.robotId,
                                                                   Capability.forTypeAndLevel(CapabilityType.HEALTH, 1))
        self.assertEqual(health_upgrade_command.type, CommandType.BUYING)
        self.assertEqual(str(health_upgrade_command.playerId), self.playerId)
        self.assertEqual(str(health_upgrade_command.data.robotId), self.robotId)
        self.assertEqual(health_upgrade_command.data.itemName, "HEALTH_1")
        self.assertIsNone(health_upgrade_command.data.targetId)
        self.assertEqual(health_upgrade_command.data.itemQuantity, 1)
        storage_upgrade_command = Command.createRobotUpgradeCommand(self.playerId, self.robotId,
                                                                    Capability.forTypeAndLevel(CapabilityType.STORAGE,
                                                                                               1))
        self.assertEqual(storage_upgrade_command.type, CommandType.BUYING)
        self.assertEqual(str(storage_upgrade_command.playerId), self.playerId)
        self.assertEqual(str(storage_upgrade_command.data.robotId), self.robotId)
        self.assertEqual(storage_upgrade_command.data.itemName, "STORAGE_1")
        self.assertIsNone(storage_upgrade_command.data.targetId)
        self.assertEqual(storage_upgrade_command.data.itemQuantity, 1)

    def test_create_robot_regenerate_command(self):
        command = Command.createRobotRegenerateCommand(self.playerId, self.robotId)
        self.assertEqual(command.type, CommandType.REGENERATE)
        self.assertEqual(str(command.playerId), self.playerId)
        self.assertEqual(str(command.data.robotId), self.robotId)
        self.assertIsNone(command.data.targetId)
        self.assertIsNone(command.data.itemQuantity)
        self.assertIsNone(command.data.itemName)

    def test_create_robot_mine_command(self):
        mine_command = Command.createRobotMineCommand(self.playerId, self.robotId, self.planetId)
        self.assertEqual(mine_command.type, CommandType.MINING)
        self.assertEqual(str(mine_command.playerId), self.playerId)
        self.assertEqual(str(mine_command.data.robotId), self.robotId)
        self.assertEqual(str(mine_command.data.planetId), self.planetId)
        self.assertIsNone(mine_command.data.targetId)
        self.assertIsNone(mine_command.data.itemQuantity)
        self.assertIsNone(mine_command.data.itemName)

    def test_create_robot_battle_command(self):
        battle_command = Command.createRobotBattleCommand(self.playerId, self.robotId, self.targetRobotId)
        self.assertEqual(battle_command.type, CommandType.BATTLE)
        self.assertEqual(str(battle_command.playerId), self.playerId)
        self.assertEqual(str(battle_command.data.robotId), self.robotId)
        self.assertEqual(str(battle_command.data.targetId), self.targetRobotId)
        self.assertIsNone(battle_command.data.itemQuantity)
        self.assertIsNone(battle_command.data.itemName)

    def test_parseToJson(self):
        command = Command.createRobotMoveCommand(self.playerId, self.robotId, self.planetId)
        json_data = command.model_dump_json()
        json_data_as_dict = dict(json.loads(json_data))
        self.assertIn("playerId", json_data)
        self.assertIn("type", json_data)
        self.assertIn("data", json_data)
        self.assertEqual("movement", json_data_as_dict["type"])
        self.assertEqual(self.playerId, json_data_as_dict["playerId"])
        self.assertEqual(self.robotId, json_data_as_dict["data"]["robotId"])
        self.assertEqual(self.planetId, json_data_as_dict["data"]["planetId"])
