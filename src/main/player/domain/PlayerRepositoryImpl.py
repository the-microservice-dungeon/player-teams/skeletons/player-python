from typing import List, Dict

from src.main.player.domain.Player import Player
from src.main.player.domain.PlayerRepository import PlayerRepository


class PlayerRepositoryImpl(PlayerRepository):

    def __init__(self):
        self.players: Dict[str, Player] = {}

    def save(self, entity) -> None:
        self.players[entity.playerId] = entity

    def get(self, id) -> Player:
        return self.players[id]

    def getAll(self) -> List[Player]:
        return list(self.players.values())

    def delete(self, id) -> None:
        del self.players[id]

    def deleteAll(self):
        self.players.clear()

    def findAll(self, predicate) -> List[Player]:
        return list(filter(predicate, self.players.values()))
