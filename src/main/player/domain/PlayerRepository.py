from abc import abstractmethod

from src.main.AbstractRepository import AbstractRepository


class PlayerRepository(AbstractRepository):
    @abstractmethod
    def findAll(self, predicate) -> list:
        pass
