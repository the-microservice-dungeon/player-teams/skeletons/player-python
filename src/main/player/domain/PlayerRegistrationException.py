from src.main.player.domain.Player import Player


class PlayerRegistrationException(Exception):
    def __init__(self, player: Player):
        super().__init__(f"Player registration for {player} failed")
