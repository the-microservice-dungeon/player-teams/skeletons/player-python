from dataclasses import dataclass
from typing import Optional


@dataclass(frozen=True)
class Player:
    playerId: Optional[str]
    gameId: Optional[str]
    name: str
    email: str
    playerQueue: str
    playerExchange: str

    def __str__(self):
        return f'Player: {self.playerId} - {self.name} - {self.email} - {self.playerQueue} - {self.playerExchange}'

    @classmethod
    def ownPlayer(cls, name: str, email: str) -> "Player":
        player = Player(
            playerId=None,
            gameId=None,
            name=name,
            email=email,
            playerQueue=f'player-{name}',
            playerExchange=f'player-{name}'
        )
        return player

    def assignPlayerId(self, playerId: str) -> "Player":
        return Player(
            playerId=playerId,
            gameId=self.gameId,
            name=self.name,
            email=self.email,
            playerQueue=self.playerQueue,
            playerExchange=self.playerExchange
        )

    def assignGameId(self, gameId: str) -> "Player":
        return Player(
            playerId=self.playerId,
            gameId=gameId,
            name=self.name,
            email=self.email,
            playerQueue=self.playerQueue,
            playerExchange=self.playerExchange
        )

    def assignNoGameId(self) -> "Player":
        return Player(
            playerId=self.playerId,
            gameId=None,
            name=self.name,
            email=self.email,
            playerQueue=self.playerQueue,
            playerExchange=self.playerExchange
        )

    def assignPlayerQueue(self, playerQueue: str) -> "Player":
        return Player(
            playerId=self.playerId,
            gameId=self.gameId,
            name=self.name,
            email=self.email,
            playerQueue=playerQueue,
            playerExchange=self.playerExchange
        )

    def assignPlayerExchange(self, playerExchange: str) -> "Player":
        return Player(
            playerId=self.playerId,
            gameId=self.gameId,
            name=self.name,
            email=self.email,
            playerQueue=self.playerQueue,
            playerExchange=playerExchange
        )

    def isRegistered(self) -> bool:
        return self.playerId is not None
