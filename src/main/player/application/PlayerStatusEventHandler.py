from src.main.core.events.GameEvent import GameEvent
from src.main.core.events.concreteevents.player.PlayerStatus import PlayerStatusPayload
from src.main.player.application.PlayerApplicationService import PlayerApplicationService


class PlayerStatusEventHandler:
    def __init__(self, playerApplicationService: PlayerApplicationService):
        self.playerApplicationService = playerApplicationService

    def handle(self, playerStatusEvent: GameEvent[PlayerStatusPayload]):
        pass

