import logging

from src.main.Config import Config
from src.main.core.restadapter.GameServiceRestAdapter import GameServiceRestAdapter
from src.main.player.domain.Player import Player
from src.main.player.domain.PlayerException import PlayerException
from src.main.player.domain.PlayerRegistrationException import PlayerRegistrationException
from src.main.player.domain.PlayerRepository import PlayerRepository


class PlayerApplicationService:

    def __init__(self, playerRepository: PlayerRepository):
        self.playerRepository = playerRepository
        self.logger = logging.getLogger(__name__)

    def __queryAndIfNeededCreatePlayer__(self) -> Player:
        players = self.playerRepository.getAll()
        if len(players) > 1:
            raise PlayerException("There is more than one player!")
        if len(players) == 1:
            return players[0]
        player = Player.ownPlayer(name=Config.PLAYER_NAME, email=Config.PLAYER_EMAIL)
        return player

    def registerPlayer(self) -> Player:
        """ Registers a Player if not already registered, saves him to the Repository and returns the Player """
        player = self.__queryAndIfNeededCreatePlayer__()
        if player.isRegistered():
            self.logger.info(f'Player already registered: {player}')
            return player
        remotePlayer = GameServiceRestAdapter.getPlayer()
        if remotePlayer is None:
            self.logger.info(f'Player not registered remotely: {player}')
            remotePlayer = GameServiceRestAdapter.registerPlayer(name=player.name, email=player.email)
            if remotePlayer is None:
                self.logger.critical(f'Failed to register player: {player}')
                raise PlayerRegistrationException(player)

        self.logger.info(f'PlayerId successfully obtained for {player.name}, Registration successful.')

        player = player.assignPlayerId(remotePlayer.playerId)
        player = player.assignPlayerQueue(remotePlayer.playerQueue)
        player = player.assignPlayerExchange(remotePlayer.playerExchange)

        self.playerRepository.save(player)
        return player

    def joinGame(self, gameId: str) -> bool:
        player: Player = self.registerPlayer()
        if player.gameId is not None:
            self.logger.info(f'{player} already in game: {player.gameId}. Cant join {gameId}')
            return False
        joinSucceeded = GameServiceRestAdapter.joinGame(gameId=gameId, playerId=player.playerId)
        if joinSucceeded:
            player = player.assignGameId(gameId=gameId)
            self.playerRepository.save(player)
            self.logger.info(f'{player} successfully joined game {gameId}')
            return True
        self.logger.warning(f'{player} failed to join game {gameId}')
        return False

    def clearGameId(self) -> bool:
        player: Player = self.registerPlayer()
        if player.gameId is None:
            self.logger.info(f'{player} not in game. Cant clear gameId.')
            return False
        player = player.assignNoGameId()
        self.playerRepository.save(player)
        self.logger.info(f'{player} successfully cleared Active gameId.')
