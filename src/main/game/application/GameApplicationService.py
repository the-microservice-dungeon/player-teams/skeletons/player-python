import logging
from typing import List, Optional

from src.main.core.restadapter.GameServiceRestAdapter import GameServiceRestAdapter
from src.main.core.restadapter.res.GameInfoResponseBody import GameInfoResponseBody
from src.main.game.domain.Game import Game
from src.main.game.domain.GameException import GameException
from src.main.game.domain.GameRepository import GameRepository
from src.main.game.domain.GameStatus import GameStatus


class GameApplicationService:
    def __init__(self, game_repository: GameRepository):
        self.gameRepository: GameRepository = game_repository
        self.logger = logging.getLogger(__name__)

    def startGame(self, gameId: str) -> None:
        game: Game = self.gameRepository.get(gameId)
        if game is None:
            self.logger.error(
                "No active game found in our Game Repository! Maybe the Game was ended by the Game Service while the Player was not started")
            return
        game.start()
        self.gameRepository.save(game)

    def endGame(self, gameId: str) -> None:
        game: Game = self.gameRepository.get(gameId)
        if game is None:
            self.logger.error(
                "No active game found in our Game Repository! Maybe the Game was ended by the Game Service while the Player was not started")
            return
        game.end()
        self.gameRepository.save(game)

    def roundStarted(self, roundNumber: int) -> None:
        game: Game = self.queryActiveGame()
        if game is None:
            self.logger.error(
                "No active game found in our Game Repository! Maybe the Game was ended by the Game Service while the Player was not started")
            return
        game.currentRoundNumber = roundNumber
        self.gameRepository.save(game)

    def fetchAndSaveRemoteGame(self) -> Optional[Game]:
        openGamesDto: List[GameInfoResponseBody] = GameServiceRestAdapter.getGames()
        if len(openGamesDto) > 1:
            raise GameException("There is more than one open game! So there must be an issue within the Game Service")
        if len(openGamesDto) == 0:
            self.logger.info("No open games found!")
            return None
        openGameDto: GameInfoResponseBody = openGamesDto[0]
        game: Game = Game.newlyCreatedGame(openGameDto.gameId)
        self.logger.info("Found open game: " + str(game))
        if openGameDto.gameStatus == GameStatus.STARTED:
            game.start()
        if openGameDto.gameStatus == GameStatus.ENDED:
            game.end()
        self.gameRepository.save(game)
        return game

    def queryActiveGame(self) -> Optional[Game]:
        games: List[Game] = self.gameRepository.findAllByGameStatusBetween(GameStatus.CREATED, GameStatus.STARTED)
        if len(games) > 1:
            self.logger.error("More than one active game found in our Game Repository!")
            raise GameException("There is more than one active game! So there must be an issue within the Game Service")
        if len(games) == 0:
            self.logger.warning("No active game found in our Game Repository!")
            return None
        return games[0]

    def queryAndIfNeededFetchRemoteGame(self) -> Optional[Game]:
        game: Game = self.queryActiveGame()
        if game is None:
            game = self.fetchAndSaveRemoteGame()
        return game
