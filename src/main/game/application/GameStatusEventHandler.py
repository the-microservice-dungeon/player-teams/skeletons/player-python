from src.main.core.events.GameEvent import GameEvent
from src.main.core.events.concreteevents.game.GameStatus import GameStatusPayload
from src.main.game.application.GameApplicationService import GameApplicationService
from src.main.game.domain.GameStatus import GameStatus
from src.main.player.application.PlayerApplicationService import PlayerApplicationService


class GameStatusEventHandler:
    def __init__(self, gameApplicationService: GameApplicationService,
                 playerApplicationService: PlayerApplicationService):
        self.gameApplicationService: GameApplicationService = gameApplicationService
        self.playerApplicationService: PlayerApplicationService = playerApplicationService

    def handle(self, event: GameEvent[GameStatusPayload]):
        self.gameApplicationService.queryAndIfNeededFetchRemoteGame()
        match event.body.status:
            case GameStatus.CREATED:
                self.gameApplicationService.fetchAndSaveRemoteGame()
                self.playerApplicationService.joinGame(gameId=event.body.gameId)
            case GameStatus.STARTED:
                self.gameApplicationService.startGame(event.body.gameId)
            case GameStatus.ENDED:
                self.gameApplicationService.endGame(event.body.gameId)
                self.playerApplicationService.clearGameId()
