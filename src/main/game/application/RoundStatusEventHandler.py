from src.main.core.events.GameEvent import GameEvent
from src.main.core.events.concreteevents.game.RoundStatus import RoundStatusPayload
from src.main.game.application.GameApplicationService import GameApplicationService


class RoundStatusEventHandler:

    def __init__(self, gameApplicationService: GameApplicationService):
        self.gameApplicationService: GameApplicationService = gameApplicationService

    def handle(self, event: GameEvent[RoundStatusPayload]):
        self.gameApplicationService.roundStarted(event.body.roundNumber)
