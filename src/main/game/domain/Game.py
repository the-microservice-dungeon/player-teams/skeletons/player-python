from dataclasses import dataclass
from typing import List

from src.main.game.domain.GameException import GameException
from src.main.game.domain.GameStatus import GameStatus


@dataclass()
class Game:
    gameId: str
    gameStatus: GameStatus
    currentRoundNumber: int
    ourPlayerHasJoined: bool

    @staticmethod
    def newlyCreatedGame(gameId: str) -> "Game":
        return Game(
            gameId=gameId,
            gameStatus=GameStatus.CREATED,
            currentRoundNumber=0,
            ourPlayerHasJoined=False
        )

    def start(self) -> None:
        if self.gameStatus != GameStatus.CREATED:
            raise GameException("Game cannot be started because it is not in CREATED state!")
        self.gameStatus = GameStatus.STARTED

    def end(self) -> None:
        if self.gameStatus != GameStatus.STARTED:
            raise GameException("Game cannot be ended because it is not in STARTED state!")
        self.gameStatus = GameStatus.ENDED

    def checkIfOurPlayerHasJoined(self, namesOfJoinedPlayers: List[str], playerName: str) -> None:
        if namesOfJoinedPlayers is None or playerName is None:
            raise GameException("namesOfJoinedPlayers || playerName cannot be null!")
        if playerName in namesOfJoinedPlayers:
            self.ourPlayerHasJoined = True
