from enum import Enum


class GameStatus(Enum):
    CREATED = "created"
    STARTED = "started"
    ENDED = "ended"

    def isOpenForJoining(self):
        return self == GameStatus.CREATED
