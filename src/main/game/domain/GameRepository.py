from abc import abstractmethod
from typing import Optional, List

from src.main.AbstractRepository import AbstractRepository
from src.main.game.domain.Game import Game
from src.main.game.domain.GameStatus import GameStatus


class GameRepository(AbstractRepository):

    @abstractmethod
    def findAllByGameStatusBetween(self, gameStatus1: GameStatus, gameStatus2: GameStatus) -> List[Game]:
        pass

    @abstractmethod
    def findAll(self, predicate) -> Optional[Game]:
        pass
