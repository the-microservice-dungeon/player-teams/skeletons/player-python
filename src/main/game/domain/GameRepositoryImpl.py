import logging
from typing import Dict, Optional, List

from src.main.game.domain.Game import Game
from src.main.game.domain.GameRepository import GameRepository
from src.main.game.domain.GameStatus import GameStatus


class GameRepositoryImpl(GameRepository):

    def __init__(self):
        self.games: Dict[str, Game] = {}
        self.logger = logging.getLogger(__name__)

    def findAllByGameStatusBetween(self, gameStatus1: GameStatus, gameStatus2: GameStatus) -> Optional[List[Game]]:
        return [game for game in self.games.values() if game.gameStatus in (gameStatus1, gameStatus2)]

    def findAll(self, predicate):
        return list(filter(predicate, self.games.values()))

    def get(self, gameId: str):
        if gameId not in self.games:
            self.logger.error("Game with id " + gameId + " not found in Repository!")
            return None
        return self.games[gameId]

    def getAll(self):
        return list(self.games.values())

    def delete(self, gameId: str):
        if gameId not in self.games:
            self.logger.error("Game with id " + gameId + " not found in Repository!")
            return
        del self.games[gameId]

    def deleteAll(self):
        self.games.clear()

    def save(self, entity: Game):
        self.games[entity.gameId] = entity
