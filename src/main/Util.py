import logging

from colorlog import ColoredFormatter

from src.main.Config import Config


def configure_logger():
    logger = logging.getLogger()
    logger.setLevel(Config.LOG_LEVEL)
    logger.handlers.clear()

    # For Log file
    file_handler = logging.FileHandler('player-python.log', mode='w')
    file_handler.setFormatter(logging.Formatter("s%(asctime)s - %(levelname)s - [%(filename)s:%(funcName)s:%(lineno)d] - %(message)s"))

    # For Console
    console_format = ColoredFormatter(
        "%(log_color)s%(asctime)s - %(lineno)d - %(levelname)s - [%(filename)s:%(funcName)s:%(lineno)d] - %(message)s",
        datefmt=None,
        reset=True,
        log_colors={
            'DEBUG': 'cyan',
            'INFO': 'green',
            'WARNING': 'yellow',
            'ERROR': 'red',
            'CRITICAL': 'red,bg_white'
        },
        secondary_log_colors={},
        style='%'
    )
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(console_format)

    logger.addHandler(file_handler)
    logger.addHandler(console_handler)
