import threading
import time
from typing import List, Optional

import pika

from src.main.Config import Config
from src.main.core.events.eventlistener.EventDispatcher import EventDispatcher
from src.main.core.events.eventlistener.RabbitMQConsumer import RabbitMQConsumer
from src.main.core.restadapter.GameServiceRestAdapter import GameServiceRestAdapter
from src.main.core.restadapter.res.GameCreatedResponseBody import GameCreatedResponseBody
from src.main.core.restadapter.res.GameInfoResponseBody import GameInfoResponseBody
from src.main.game.application.GameApplicationService import GameApplicationService
from src.main.game.domain.Game import Game
from src.main.game.domain.GameRepositoryImpl import GameRepositoryImpl
from src.main.game.domain.GameStatus import GameStatus
from src.main.player.application.PlayerApplicationService import PlayerApplicationService
from src.main.player.domain.Player import Player
from src.main.player.domain.PlayerRepositoryImpl import PlayerRepositoryImpl
from src.main.Util import configure_logger


class DungeonPlayerStartupHandler:
    def __init__(self):
        configure_logger()
        self.playerApplicationService = PlayerApplicationService(PlayerRepositoryImpl())
        self.gameApplicationService = GameApplicationService(GameRepositoryImpl())
        self.rabbitMQConsumer = self.__setupRabbitMQConsumer()

    def start(self):
        player = self.playerApplicationService.registerPlayer()
        self.__handleDevMode()
        self.__startRabbitMQConsumer(player)

    def __handleDevMode(self):
        if Config.DEV:
            GameServiceRestAdapter.endAllActiveGames()
            self.__createOneGameAndStartItOnceOnePlayerJoined()

    def __startRabbitMQConsumer(self, player: Player) -> None:
        self.rabbitMQConsumer.purge_queue(player.playerQueue)
        potentialGame = self.gameApplicationService.fetchAndSaveRemoteGame()
        if potentialGame is not None:
            self.playerApplicationService.joinGame(potentialGame.gameId)
        self.rabbitMQConsumer.start_event_listening_and_handling(player.playerQueue)

    def __setupRabbitMQConsumer(self) -> RabbitMQConsumer:
        credentials = pika.PlainCredentials(username=Config.RABBITMQ_USERNAME, password=Config.RABBITMQ_PASSWORD)
        connectionParameters = pika.ConnectionParameters(host=Config.RABBITMQ_HOST, port=Config.RABBITMQ_PORT,
                                                         credentials=credentials)
        connection = pika.BlockingConnection(connectionParameters)
        channel = connection.channel()
        return RabbitMQConsumer(connection, channel, self.__setupEventDispatcher())

    def __setupEventDispatcher(self) -> EventDispatcher:
        return EventDispatcher(self.gameApplicationService, self.playerApplicationService)

    def __createOneGameAndStartItOnceOnePlayerJoined(self) -> None:
        gameCreatedResponse = GameServiceRestAdapter.createGame(maxPlayers=2, maxRounds=200)
        if gameCreatedResponse is not None:
            GameServiceRestAdapter.patchRoundTime(gameId=gameCreatedResponse.gameId, durationInMillis=10000)
            threading.Thread(target=self.__waitForPlayerJoinAndThenStartGame, kwargs={
                'gameId': gameCreatedResponse.gameId,
                'numPlayers': 1
            }, daemon=True).start()

    def __waitForPlayerJoinAndThenStartGame(self, gameId: str, numPlayers: int = 1) -> None:
        while True:
            game = GameServiceRestAdapter.getGame(gameId)
            if game and game.gameStatus == GameStatus.CREATED.value and len(game.participatingPlayers) >= numPlayers:
                GameServiceRestAdapter.startGame(game.gameId)
                exit(0)
            time.sleep(3)
