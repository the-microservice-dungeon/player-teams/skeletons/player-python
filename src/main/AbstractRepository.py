from abc import abstractmethod, ABC


class AbstractRepository(ABC):
    @abstractmethod
    def save(self, entity):
        pass

    @abstractmethod
    def get(self, id):
        pass

    @abstractmethod
    def getAll(self):
        pass

    @abstractmethod
    def delete(self, id):
        pass

    @abstractmethod
    def deleteAll(self):
        pass
