import random
from enum import Enum


class CompassDirection(Enum):
    NORTH = "NORTH"
    EAST = "EAST"
    SOUTH = "SOUTH"
    WEST = "WEST"

    def getOppositeDirection(self) -> "CompassDirection":
        match self:
            case CompassDirection.NORTH:
                return CompassDirection.SOUTH
            case CompassDirection.EAST:
                return CompassDirection.WEST
            case CompassDirection.SOUTH:
                return CompassDirection.NORTH
            case CompassDirection.WEST:
                return CompassDirection.EAST

    def xOffset(self) -> int:
        match self:
            case CompassDirection.NORTH:
                return 0
            case CompassDirection.EAST:
                return 1
            case CompassDirection.SOUTH:
                return 0
            case CompassDirection.WEST:
                return -1

    def yOffset(self) -> int:
        match self:
            case CompassDirection.NORTH:
                return -1
            case CompassDirection.EAST:
                return 0
            case CompassDirection.SOUTH:
                return 1
            case CompassDirection.WEST:
                return 0

    def ninetyDegrees(self) -> ["CompassDirection"]:
        match self:
            case CompassDirection.NORTH:
                return [CompassDirection.EAST, CompassDirection.WEST]
            case CompassDirection.EAST:
                return [CompassDirection.NORTH, CompassDirection.SOUTH]
            case CompassDirection.SOUTH:
                return [CompassDirection.EAST, CompassDirection.WEST]
            case CompassDirection.WEST:
                return [CompassDirection.NORTH, CompassDirection.SOUTH]

    @staticmethod
    def random() -> "CompassDirection":
        return random.choice(list(CompassDirection))
