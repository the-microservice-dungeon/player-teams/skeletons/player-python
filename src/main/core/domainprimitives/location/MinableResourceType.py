from enum import Enum


class MinableResourceType(Enum):
    COAL = "COAL"
    IRON = "IRON"
    GEM = "GEM"
    GOLD = "GOLD"
    PLATIN = "PLATIN"
