from dataclasses import dataclass

from src.main.core.domainprimitives.DomainPrimitiveException import DomainPrimitiveException
from src.main.core.domainprimitives.location.MinableResourceType import MinableResourceType


@dataclass(frozen=True)
class MinableResource:
    type: MinableResourceType
    amount: int

    @staticmethod
    def fromTypeAndAmount(minableResourceType: MinableResourceType, amount: int) -> "MinableResource":
        if minableResourceType is None:
            raise DomainPrimitiveException("MinableResourceType cannot be null!")
        if amount is None:
            raise DomainPrimitiveException("Amount cannot be null!")
        if amount <= 0:
            raise DomainPrimitiveException("Amount must be > 0!")
        return MinableResource(minableResourceType, amount)

    def add(self, additionalResource: "MinableResource") -> "MinableResource":
        if additionalResource is None:
            raise DomainPrimitiveException("additionalResource cannot be null!")
        if additionalResource.isEmpty():
            return self
        if self.isEmpty():
            return additionalResource
        if self.type != additionalResource.type:
            raise DomainPrimitiveException("Cannot add resources of different types!")
        return MinableResource(self.type, self.amount + additionalResource.amount)

    def isEmpty(self) -> bool:
        return self.amount == 0
