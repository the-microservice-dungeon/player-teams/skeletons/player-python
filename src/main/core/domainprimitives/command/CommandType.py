from enum import Enum


class CommandType(Enum):
    MOVEMENT = "movement"
    BATTLE = "battle"
    MINING = "mining"
    REGENERATE = "regenerate"
    BUYING = "buying"
    SELLING = "selling"

    def __str__(self) -> str:
        return self.value
