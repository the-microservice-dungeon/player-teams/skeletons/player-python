from typing import Optional
from uuid import UUID

from pydantic import BaseModel


class CommandObject(BaseModel):
    robotId: Optional[UUID] = None
    planetId: Optional[UUID] = None
    targetId: Optional[UUID] = None
    itemName: Optional[str] = None
    itemQuantity: Optional[int] = None

    class Config:
        extra = "forbid"
        validate_assignment = True
        json_encoders = {
            UUID: lambda uuid_: str(uuid_)
        }
        json_decoders = {
            UUID: lambda uuid_: UUID(uuid_)
        }
