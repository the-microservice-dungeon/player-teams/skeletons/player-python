from dataclasses import dataclass
from uuid import UUID

from pydantic import BaseModel, Field

from src.main.core.domainprimitives.DomainPrimitiveException import DomainPrimitiveException
from src.main.core.domainprimitives.command.CommandObject import CommandObject
from src.main.core.domainprimitives.command.CommandType import CommandType
from src.main.core.domainprimitives.purchasing.Capability import Capability
from src.main.core.domainprimitives.purchasing.ItemType import ItemType


@dataclass(frozen=True)
class Command(BaseModel):
    playerId: UUID
    data: CommandObject
    type: CommandType = Field(alias="commandType")

    class Config:
        validate_assignment = True
        extra = "forbid"
        populate_by_name = True
        json_encoders = {
            UUID: lambda uuid_: str(uuid_)
        }
        json_decoders = {
            UUID: lambda uuid_: UUID(uuid_)
        }

    @classmethod
    def createRobotMoveCommand(cls, playerId: str, robotId: str, planetId: str) -> "Command":
        return cls(
            playerId=UUID(playerId),
            data=CommandObject(robotId=UUID(robotId), planetId=UUID(planetId)),
            type=CommandType.MOVEMENT
        )

    @classmethod
    def createBuyRobotHealthRestoreCommand(cls, playerId: str, robotId: str) -> "Command":
        return cls(
            playerId=UUID(playerId),
            data=CommandObject(robotId=UUID(robotId), itemName=ItemType.HEALTH_RESTORE, itemQuantity=1),
            type=CommandType.BUYING
        )

    @classmethod
    def createBuyRobotEnergyRestoreCommand(cls, playerId: str, robotId: str) -> "Command":
        return cls(
            playerId=UUID(playerId),
            data=CommandObject(robotId=UUID(robotId), itemName=ItemType.ENERGY_RESTORE, itemQuantity=1),
            type=CommandType.BUYING
        )

    @classmethod
    def createRobotBuyCommand(cls, playerId: str, amount: int) -> "Command":
        if amount <= 0:
            raise DomainPrimitiveException("Amount must be > 0")
        return cls(
            playerId=UUID(playerId),
            data=CommandObject(itemName=ItemType.ROBOT.value, itemQuantity=amount),
            type=CommandType.BUYING
        )

    @classmethod
    def createSellInventoryCommand(cls, playerId: str, robotId: str) -> "Command":
        return cls(
            playerId=UUID(playerId),
            data=CommandObject(robotId=UUID(robotId)),
            type=CommandType.SELLING
        )

    @classmethod
    def createRobotUpgradeCommand(cls, playerId: str, robotId: str, capability: Capability) -> "Command":
        return cls(
            playerId=UUID(playerId),
            data=CommandObject(robotId=UUID(robotId), itemName=capability.toStringForCommand(), itemQuantity=1),
            type=CommandType.BUYING
        )

    @classmethod
    def createRobotRegenerateCommand(cls, playerId: str, robotId: str) -> "Command":
        return cls(
            playerId=UUID(playerId),
            data=CommandObject(robotId=UUID(robotId)),
            type=CommandType.REGENERATE
        )

    @classmethod
    def createRobotMineCommand(cls, playerId: str, robotId: str, planetId: str) -> "Command":
        return cls(
            playerId=UUID(playerId),
            data=CommandObject(robotId=UUID(robotId), planetId=UUID(planetId)),
            type=CommandType.MINING
        )

    @classmethod
    def createRobotBattleCommand(cls, playerId: str, robotId: str, targetId: str) -> "Command":
        return cls(
            playerId=UUID(playerId),
            data=CommandObject(robotId=UUID(robotId), targetId=UUID(targetId)),
            type=CommandType.BATTLE
        )
