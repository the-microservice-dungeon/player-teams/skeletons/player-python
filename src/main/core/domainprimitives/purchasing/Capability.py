from dataclasses import dataclass
from typing import List

from src.main.core.domainprimitives.DomainPrimitiveException import DomainPrimitiveException
from src.main.core.domainprimitives.purchasing.CapabilityType import CapabilityType


@dataclass(frozen=True)
class Capability:
    capabilityType: CapabilityType
    level: int
    MIN_LEVEL: int = 0
    MAX_LEVEL: int = 5

    @staticmethod
    def baseForType(capabilityType: CapabilityType) -> "Capability":
        return Capability.forTypeAndLevel(capabilityType, Capability.MIN_LEVEL)

    @staticmethod
    def forTypeAndLevel(capabilityType: CapabilityType, level: int) -> "Capability":
        if capabilityType is None or level is None:
            raise DomainPrimitiveException("capabilityType == null || level == null")
        if level < Capability.MIN_LEVEL or level > Capability.MAX_LEVEL:
            raise DomainPrimitiveException("level < MIN_LEVEL || level > MAX_LEVEL")
        return Capability(capabilityType, level)

    @staticmethod
    def allBaseCapabilities() -> List["Capability"]:
        return [Capability.baseForType(capabilityType) for capabilityType in CapabilityType]

    def nextLevel(self) -> "Capability":
        if self.level < self.MAX_LEVEL:
            return Capability.forTypeAndLevel(self.capabilityType, self.level + 1)
        else:
            return None

    def isMinimumLevel(self) -> bool:
        return self.level == self.MIN_LEVEL

    def isMaximumLevel(self) -> bool:
        return self.level == self.MAX_LEVEL

    def __str__(self):
        return f"{self.capabilityType}-{self.level}"

    def toStringForCommand(self) -> str:
        return f"{self.capabilityType.name}_{self.level}"
