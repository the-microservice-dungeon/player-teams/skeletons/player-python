from enum import Enum


class TradeableType(Enum):
    ITEM = "ITEM"
    UPGRADE = "UPGRADE"
    RESTORATION = "RESTORATION"
    RESOURCE = "RESOURCE"
