from pydantic import BaseModel

from src.main.core.domainprimitives.purchasing.TradeableType import TradeableType


class TradeableItem(BaseModel):
    name: str
    price: int
    type: TradeableType