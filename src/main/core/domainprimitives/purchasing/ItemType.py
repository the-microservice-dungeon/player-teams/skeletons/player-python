from enum import Enum


class ItemType(Enum):
    HEALTH_RESTORE = "HEALTH_RESTORE"
    ENERGY_RESTORE = "ENERGY_RESTORE"
    ROBOT = "ROBOT"
