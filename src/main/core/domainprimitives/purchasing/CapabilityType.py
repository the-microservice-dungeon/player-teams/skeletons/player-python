from enum import Enum


class CapabilityType(Enum):
    DAMAGE = "DA"
    ENERGY_REGEN = "ER"
    HEALTH = "H"
    MAX_ENERGY = "ME"
    MINING = "MI"
    MINING_SPEED = "MS"
    STORAGE = "S"

    def __str__(self) -> str:
        return self.value
