from dataclasses import dataclass

from src.main.core.domainprimitives.DomainPrimitiveException import DomainPrimitiveException


@dataclass(frozen=True)
class Money:
    amount: int

    @staticmethod
    def fromAmount(amount_as_int: int) -> "Money":
        if amount_as_int is None:
            raise DomainPrimitiveException("Amount cannot be null!")
        if amount_as_int < 0:
            raise DomainPrimitiveException("Amount must be >= 0!")
        return Money(amount_as_int)

    @staticmethod
    def zero() -> "Money":
        return Money.fromAmount(0)

    def canBuyThatManyFor(self, price: "Money") -> int:
        if price is None or price.amount is None:
            raise DomainPrimitiveException("price == null")
        return self.amount // price.amount

    def decreaseBy(self, money_due: "Money") -> "Money":
        if money_due is None:
            raise DomainPrimitiveException("amountDue == null")
        if money_due.greaterThan(self):
            raise DomainPrimitiveException("Cannot decrease by more than it has!")
        return Money.fromAmount(self.amount - money_due.amount)

    def increaseBy(self, additional_money: "Money") -> "Money":
        if additional_money is None:
            raise DomainPrimitiveException("additionalAmount == null")
        return Money.fromAmount(self.amount + additional_money.amount)

    def greaterThan(self, other_money: "Money") -> bool:
        if other_money is None:
            raise DomainPrimitiveException("otherMoney == null")
        return self.amount > other_money.amount

    def greaterEqualThan(self, other_money: "Money") -> bool:
        if other_money is None:
            raise DomainPrimitiveException(">=: otherMoney == null")
        return self.amount >= other_money.amount

    def __str__(self):
        return f"{self.amount}€"
