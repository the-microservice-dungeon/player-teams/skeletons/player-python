from dataclasses import dataclass

from src.main.core.domainprimitives.DomainPrimitiveException import DomainPrimitiveException


@dataclass(frozen=True)
class Energy:
    energyAmount: int

    @staticmethod
    def fromAmount(amount: int) -> "Energy":
        if amount is None:
            raise DomainPrimitiveException("Energy amount cannot be null!")
        if amount < 0:
            raise DomainPrimitiveException("Energy amount must be >= 0!")
        return Energy(amount)

    def zero(self) -> "Energy":
        return self.fromAmount(0)

    def increaseBy(self, energy: "Energy") -> "Energy":
        if energy is None:
            raise DomainPrimitiveException("Energy cannot be increased by null!")
        return Energy.fromAmount(self.energyAmount + energy.energyAmount)

    def decreaseBy(self, energy: "Energy") -> "Energy":
        if energy is None:
            raise DomainPrimitiveException("Energy cannot be decreased by null!")
        if self.energyAmount < energy.energyAmount:
            raise DomainPrimitiveException("Energy cannot be decreased by more than it has!")
        return Energy.fromAmount(self.energyAmount - energy.energyAmount)

    def greaterThan(self, energy: "Energy") -> bool:
        if energy is None:
            raise DomainPrimitiveException("Energy cannot be compared to null!")
        return self.energyAmount > energy.energyAmount

    def greaterThanOrEqual(self, energy: "Energy") -> bool:
        if energy is None:
            raise DomainPrimitiveException("Energy cannot be compared to null!")
        return self.energyAmount >= energy.energyAmount

    def lowerPercentageThan(self, percentage: int, comparisonEnergy: "Energy") -> bool:
        if comparisonEnergy is None:
            raise DomainPrimitiveException("Energy cannot be compared to null!")
        if percentage < 0 or percentage > 100:
            raise DomainPrimitiveException("Percentage must be between 0 and 100!")
        fraction: float = float(comparisonEnergy.energyAmount) / (float(percentage) / float(100))
        return float(self.energyAmount) < fraction
