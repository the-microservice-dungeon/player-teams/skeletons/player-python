from typing import Optional

from pydantic import BaseModel


class GameEventHeader(BaseModel):
    type: Optional[str]
    eventId: Optional[str]
    transactionId: Optional[str]
    version: Optional[int]
    playerId: Optional[str]
    timestamp: Optional[str]

    class Config:
        ignore_extra = True