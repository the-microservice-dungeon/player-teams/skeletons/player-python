import logging

from src.main.core.events.GameEvent import GameEvent
from src.main.core.events.concreteevents.game.GameStatus import GameStatusPayload
from src.main.core.events.concreteevents.game.RoundStatus import RoundStatusPayload
from src.main.core.events.concreteevents.player.PlayerStatus import PlayerStatusPayload
from src.main.game.application.GameApplicationService import GameApplicationService
from src.main.game.application.GameStatusEventHandler import GameStatusEventHandler
from src.main.game.application.RoundStatusEventHandler import RoundStatusEventHandler
from src.main.player.application.PlayerApplicationService import PlayerApplicationService
from src.main.player.application.PlayerStatusEventHandler import PlayerStatusEventHandler


class EventDispatcher:
    def __init__(self, gameApplicationService: GameApplicationService,
                 playerApplicationService: PlayerApplicationService):
        self.logger = logging.getLogger(__name__)
        self.handlers = {
            # Game
            GameStatusPayload: GameStatusEventHandler(gameApplicationService, playerApplicationService),
            RoundStatusPayload: RoundStatusEventHandler(gameApplicationService),
            # Robot

            # Player
            PlayerStatusPayload: PlayerStatusEventHandler(playerApplicationService),

            # Map

            # Trading
            # TODO: Handler für die unterschiedlichen Events hier eintragen

        }

    def dispatch(self, event: GameEvent):
        handler = self.handlers.get(type(event.body))
        if handler:
            handler.handle(event)
        else:
            self.logger.warning(f"No Event Handler found for event {event.header.type}\n{event.body}")
