import logging
import time

from pika import BlockingConnection, spec
from pika.adapters.blocking_connection import BlockingChannel

from src.main.core.events.GameEvent import GameEvent
from src.main.core.events.eventlistener.EventDeserializer import deserializeEvent
from src.main.core.events.eventlistener.EventDispatcher import EventDispatcher


class RabbitMQConsumer:

    def __init__(self, connection: BlockingConnection, channel: BlockingChannel, eventDispatcher: EventDispatcher):
        self.connection = connection
        self.channel = channel
        self.logger = logging.getLogger(__name__)
        self.eventDispatcher = eventDispatcher

    def on_message(self, channel: BlockingChannel, method_frame: spec.Basic.Deliver, properties: spec.BasicProperties, body: bytes):
        try:
            header = properties.headers
            self.logger.info(f"Received message: Header={header}, Body={body}")

            event: GameEvent = deserializeEvent(header, body)
            if event is None:
                self.logger.error(f"Event with type {header.get('type')} could not be deserialized.")
                return

            self.eventDispatcher.dispatch(event)
            channel.basic_ack(delivery_tag=method_frame.delivery_tag)
        except Exception as e:
            self.logger.exception("Error processing message", exc_info=e)

    def start_event_listening_and_handling(self, playerQueue: str):
        self.logger.info("Listening for RabbitMQ events...")
        self.channel.basic_consume(playerQueue, self.on_message)
        try:
            self.channel.start_consuming()
        except KeyboardInterrupt:
            self.logger.info("Interrupt received, stopping consumer")
            self.channel.stop_consuming()
        except Exception as e:
            self.logger.exception("Error in the consuming loop", exc_info=e)
        finally:
            self.connection.close()

    def purge_queue(self, queue: str):
        self.logger.info(f"Purging queue: {queue}")
        self.channel.queue_purge(queue)
        time.sleep(1)  # Konstante für Delay festlegen