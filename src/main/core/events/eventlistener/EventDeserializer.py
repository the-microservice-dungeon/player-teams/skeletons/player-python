import json
import logging
from typing import Type, Mapping, Any

from src.main.core.events.GameEvent import GameEvent
from src.main.core.events.GameEventBody import GameEventBody
from src.main.core.events.concreteevents.game.GameStatus import GameStatusPayload
from src.main.core.events.concreteevents.game.RoundStatus import RoundStatusPayload
from src.main.core.events.concreteevents.map.PlanetDiscovered import PlanetDiscoveredPayload
from src.main.core.events.concreteevents.map.PlanetResourceMined import PlanetResourceMinedPayload
from src.main.core.events.concreteevents.player.PlayerStatus import PlayerStatusPayload
from src.main.core.events.concreteevents.robot.mine.RobotResourceMined import RobotResourceMinedPayload
from src.main.core.events.concreteevents.robot.move.RobotMovedPayload import RobotMovedPayload
from src.main.core.events.concreteevents.robot.reveal.RobotsRevealed import RobotsRevealedPayload
from src.main.core.events.concreteevents.robot.spawn.RobotSpawned import RobotSpawnedPayload
from src.main.core.events.concreteevents.trading.BankAccountCleared import BankAccountClearedPayload
from src.main.core.events.concreteevents.trading.BankAccountInitialized import BankAccountInitializedPayload
from src.main.core.events.concreteevents.trading.BankAccountTransactionBooked import BankAccountTransactionBookedPayload
from src.main.core.events.concreteevents.trading.TradablePrices import TradeablePricesPayload

# Mapping von Event-Typen zu Pydantic-Modellen
event_model_mapping: dict[str, Type[GameEventBody]] = {
    # Game
    "RoundStatus": RoundStatusPayload,
    "GameStatus": GameStatusPayload,
    # Robot
    "RobotMoved": RobotMovedPayload,
    "RobotSpawned": RobotSpawnedPayload,
    "RobotResourceMined": RobotResourceMinedPayload,
    "RobotsRevealed": RobotsRevealedPayload,

    # Map
    "PlanetDiscovered": PlanetDiscoveredPayload,
    "PlanetResourceMined": PlanetResourceMinedPayload,

    # Player
    "PlayerStatus": PlayerStatusPayload,

    # Trading
    "TradablePrices": TradeablePricesPayload,
    "BankAccountInitialized": BankAccountInitializedPayload,
    "BankAccountCleared": BankAccountClearedPayload,
    "BankAccountTransactionBooked": BankAccountTransactionBookedPayload
    # TODO: Weitere Event-Typen hinzufügen
}


def deserializeEvent(header: Mapping[str, Any], body: bytes) -> GameEvent:
    header = {key: value.decode('utf-8') if isinstance(value, bytes) else value for key, value in header.items()}

    event_type = header.get("type")

    event_model = event_model_mapping.get(event_type)

    if event_model:
        if event_type == "TradablePrices":
            # Specialcase for TradablePrices
            tradeables_list = json.loads(body.decode('utf-8'))
            event_body = TradeablePricesPayload.from_list(tradeables_list)
        else:
            # Default Deserialization
            body_dict = json.loads(body.decode('utf-8'))
            event_body = event_model(**body_dict)

        return GameEvent(header=header, body=event_body)
    else:
        logging.getLogger(__name__).error(f"Unknown Event type: {event_type}")
