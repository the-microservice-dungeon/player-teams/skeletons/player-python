from src.main.core.events.GameEventBody import GameEventBody


class PlayerStatusPayload(GameEventBody):
    playerId: str
    gameId: str
    name: str
