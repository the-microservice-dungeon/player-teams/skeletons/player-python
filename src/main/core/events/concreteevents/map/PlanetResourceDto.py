from pydantic import BaseModel

from src.main.core.domainprimitives.location.MinableResourceType import MinableResourceType


class PlanetResourceDto(BaseModel):
    resourceType: MinableResourceType
    maxAmount: int
    currentAmount: int
