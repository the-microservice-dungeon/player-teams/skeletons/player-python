from pydantic import BaseModel

from src.main.core.domainprimitives.location.CompassDirection import CompassDirection


class PlanetNeighbourDto(BaseModel):
    planetId: str
    direction: CompassDirection
