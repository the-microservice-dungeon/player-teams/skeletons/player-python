from pydantic import Field

from src.main.core.events.GameEventBody import GameEventBody
from src.main.core.events.concreteevents.map.PlanetResourceDto import PlanetResourceDto


class PlanetResourceMinedPayload(GameEventBody):
    planetId: str = Field(alias="planet")
    minedAmount: int
    resource: PlanetResourceDto
