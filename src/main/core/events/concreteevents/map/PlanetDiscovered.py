from typing import List

from src.main.core.events.GameEventBody import GameEventBody
from src.main.core.events.concreteevents.map.PlanetNeighbourDto import PlanetNeighbourDto
from src.main.core.events.concreteevents.map.PlanetResourceDto import PlanetResourceDto


class PlanetDiscoveredPayload(GameEventBody):
    planetId: str
    movementDifficulty: int
    neighbours: List[PlanetNeighbourDto]
    resource: PlanetResourceDto
