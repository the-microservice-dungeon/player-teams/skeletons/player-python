from typing import Optional

from pydantic import Field

from src.main.core.events.GameEventBody import GameEventBody
from src.main.game.domain.GameStatus import GameStatus


class GameStatusPayload(GameEventBody):
    gameId: str
    gameWorldId: Optional[str] = Field(alias='gameworldId')
    status: GameStatus

    class Config:
        populate_by_names = True