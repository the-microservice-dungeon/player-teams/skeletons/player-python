from enum import Enum
from typing import Optional

from pydantic import BaseModel

from src.main.core.events.GameEventBody import GameEventBody


class ImpreciseTimings(BaseModel):
    roundStarted: str
    commandInputEnded: Optional[str]
    roundEnded: Optional[str]


class ImpreciseTimingPredictions(BaseModel):
    roundStart: str
    commandInputEnd: str
    roundEnd: str


class RoundStatusType(Enum):
    STARTED = "started"
    COMMAND_INPUT_ENDED = "command input ended"
    ENDED = "ended"


class RoundStatusPayload(GameEventBody):
    gameId: str
    roundId: str
    roundNumber: int
    roundStatus: RoundStatusType
    impreciseTimings: ImpreciseTimings
    impreciseTimingPredictions: ImpreciseTimingPredictions
