from src.main.core.events.GameEventBody import GameEventBody


class BankAccountInitializedPayload(GameEventBody):
    playerId: str
    balance: float
