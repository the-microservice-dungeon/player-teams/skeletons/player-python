from src.main.core.events.GameEventBody import GameEventBody


class BankAccountTransactionBookedPayload(GameEventBody):
    playerId: str
    balance: float
    transactionAmount: float
