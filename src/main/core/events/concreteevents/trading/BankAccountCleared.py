from src.main.core.events.GameEventBody import GameEventBody


class BankAccountClearedPayload(GameEventBody):
    playerId: str
    balance: float
