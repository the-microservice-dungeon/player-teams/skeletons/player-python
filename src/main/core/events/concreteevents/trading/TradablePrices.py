from typing import List

from src.main.core.domainprimitives.purchasing.TradeableItem import TradeableItem
from src.main.core.events.GameEventBody import GameEventBody


class TradeablePricesPayload(GameEventBody):
    tradeables: List[TradeableItem]

    @classmethod
    def from_list(cls, tradeables_list: List[dict]) -> "TradeablePricesPayload":
        tradeables: List[TradeableItem] = [TradeableItem(**item) for item in tradeables_list]
        return cls(tradeables=tradeables)
