from src.main.core.events.GameEventBody import GameEventBody


class RobotRegeneratedPayload(GameEventBody):
    robotId: str
    availableEnergy: int
