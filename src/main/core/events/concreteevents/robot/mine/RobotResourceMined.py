import logging

from src.main.core.domainprimitives.location.MinableResource import MinableResource
from src.main.core.domainprimitives.location.MinableResourceType import MinableResourceType
from src.main.core.events.GameEventBody import GameEventBody
from src.main.core.events.concreteevents.robot.mine.RobotResourceInventoryDto import RobotResourceInventoryDto


class RobotResourceMinedPayload(GameEventBody):
    robotId: str
    minedAmount: int
    minedResource: MinableResourceType
    resourceInventory: RobotResourceInventoryDto

    def minedResourceAsDomainPrimitive(self) -> MinableResource:
        try:
            return MinableResource.fromTypeAndAmount(self.minedResource, self.minedAmount)
        except Exception as e:
            logging.getLogger(__name__).error("Could not convert minedResource to MineableResource  " + str(e))
