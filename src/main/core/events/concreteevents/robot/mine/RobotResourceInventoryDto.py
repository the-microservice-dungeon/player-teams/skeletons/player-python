from typing import Optional

from pydantic import BaseModel, Field

from src.main.core.domainprimitives.location.MinableResource import MinableResource
from src.main.core.domainprimitives.location.MinableResourceType import MinableResourceType


class RobotResourceInventoryDto(BaseModel):
    coal: int = Field(alias="COAL")
    iron: int = Field(alias="IRON")
    gem: int = Field(alias="GEM")
    gold: int = Field(alias="GOLD")
    platin: int = Field(alias="PLATIN")

    def getResource(self) -> Optional[MinableResource]:
        if self.coal > 0:
            return MinableResource.fromTypeAndAmount(MinableResourceType.COAL, self.coal)
        if self.iron > 0:
            return MinableResource.fromTypeAndAmount(MinableResourceType.IRON, self.iron)
        if self.gem > 0:
            return MinableResource.fromTypeAndAmount(MinableResourceType.GEM, self.gem)
        if self.gold > 0:
            return MinableResource.fromTypeAndAmount(MinableResourceType.GOLD, self.gold)
        if self.platin > 0:
            return MinableResource.fromTypeAndAmount(MinableResourceType.PLATIN, self.platin)
        return None

    class Config:
        populate_by_name = True
