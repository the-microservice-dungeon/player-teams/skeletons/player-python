from pydantic import BaseModel


class RobotMovePlanetDto(BaseModel):
    id: str
    movementDifficulty: int
