from src.main.core.events.GameEventBody import GameEventBody
from src.main.core.events.concreteevents.robot.move.RobotMovePlanetDto import RobotMovePlanetDto


class RobotMovedPayload(GameEventBody):
    robotId: str
    remainingEnergy: int
    fromPlanet: RobotMovePlanetDto
    toPlanet: RobotMovePlanetDto
