from pydantic import BaseModel, Field

from src.main.core.domainprimitives.location.MinableResourceType import MinableResourceType


class RobotPlanetDto(BaseModel):
    planetId: str
    gameWorldId: str = Field(alias='gameworldId')
    movementDifficulty: int
    resourceType: MinableResourceType

    class Config:
        populate_by_name = True
