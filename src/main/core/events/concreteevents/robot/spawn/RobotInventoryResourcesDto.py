from pydantic import BaseModel


class RobotInventoryResourcesDto(BaseModel):
    coal: int
    iron: int
    gem: int
    gold: int
    platin: int