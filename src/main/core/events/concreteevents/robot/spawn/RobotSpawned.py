from src.main.core.events.GameEventBody import GameEventBody
from src.main.core.events.concreteevents.robot.spawn.RobotDto import RobotDto


class RobotSpawnedPayload(GameEventBody):
    playerId: str
    robotDto: RobotDto
