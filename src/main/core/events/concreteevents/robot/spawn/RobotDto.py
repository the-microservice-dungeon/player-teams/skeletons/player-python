from pydantic import BaseModel, Field

from src.main.core.events.concreteevents.robot.spawn.RobotInventoryDto import RobotInventoryDto
from src.main.core.events.concreteevents.robot.spawn.RobotPlanetDto import RobotPlanetDto


class RobotDto(BaseModel):
    playerId: str = Field(alias='player')
    planet: RobotPlanetDto
    robotId: str = Field(alias='id')
    alive: bool
    inventory: RobotInventoryDto
    health: int
    energy: int

    healthLevel: int
    damageLevel: int
    miningSpeedLevel: int
    miningLevel: int
    energyLevel: int
    energyRegenLevel: int

    miningSpeed: int
    maxHealth: int
    maxEnergy: int
    energyRegen: int
    damage: int

    class Config:
        populate_by_name = True