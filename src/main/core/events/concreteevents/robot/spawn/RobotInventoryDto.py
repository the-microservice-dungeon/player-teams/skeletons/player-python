from pydantic import BaseModel

from src.main.core.events.concreteevents.robot.spawn.RobotInventoryResourcesDto import RobotInventoryResourcesDto


class RobotInventoryDto(BaseModel):
    storageLevel: int
    usedStorage: int
    resources: RobotInventoryResourcesDto
    full: bool
    maxStorage:int