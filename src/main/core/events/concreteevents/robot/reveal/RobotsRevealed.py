from typing import List

from src.main.core.events.GameEventBody import GameEventBody
from src.main.core.events.concreteevents.robot.reveal.RobotRevealedDto import RobotRevealedDto


class RobotsRevealedPayload(GameEventBody):
    robots: List[RobotRevealedDto]
