from pydantic import BaseModel


class RobotRevealedLevelDto(BaseModel):
    healthLevel: int
    damageLevel: int
    miningSpeedLevel: int
    miningLevel: int
    energyLevel: int
    energyRegenLevel: int
    storageLevel: int
