from pydantic import BaseModel

from src.main.core.events.concreteevents.robot.reveal.RobotRevealedLevelDto import RobotRevealedLevelDto


class RobotRevealedDto(BaseModel):
    robotId: str
    planetId: str
    playerNotion: str
    health: int
    energy: int
    levels: RobotRevealedLevelDto
