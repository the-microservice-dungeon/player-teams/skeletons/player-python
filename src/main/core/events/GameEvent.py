from typing import Generic

from pydantic import BaseModel

from src.main.core.events.GameEventBody import T
from src.main.core.events.GameEventHeader import GameEventHeader


class GameEvent(BaseModel, Generic[T]):
    header: GameEventHeader
    body: T
