from typing import TypeVar

from pydantic import BaseModel


class GameEventBody(BaseModel):
    pass


T = TypeVar('T', bound=GameEventBody)
