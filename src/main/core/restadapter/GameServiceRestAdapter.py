import logging
import time
from http import HTTPStatus
from typing import Optional, List

import requests

from src.main.Config import Config
from src.main.core.domainprimitives.command.Command import Command
from src.main.core.restadapter.RestAdapterException import RestAdapterException
from src.main.core.restadapter.req.CreateGameRequestBody import CreateGameRequestBody
from src.main.core.restadapter.req.PatchRoundTimeRequestBody import PatchRoundTimeRequestBody
from src.main.core.restadapter.req.PlayerRegistrationRequestBody import PlayerRegistrationRequestBody
from src.main.core.restadapter.res.GameCreatedResponseBody import GameCreatedResponseBody
from src.main.core.restadapter.res.GameInfoResponseBody import GameInfoResponseBody
from src.main.core.restadapter.res.PlayerRegistrationResponseBody import PlayerRegistrationResponseBody
from src.main.game.domain.GameStatus import GameStatus

gameHost = f'{Config.GAME_HOST}:{Config.GAME_PORT}'


class GameServiceRestAdapter:
    _logger = logging.getLogger(__name__)

    @classmethod
    def __handleUnknownError(cls, response):
        error_message = response.json().get('message', 'No error message provided')
        cls._logger.critical(
            f'Unknown Error while calling Game Service {response.url} - {response.status_code} - {error_message}')
        raise RestAdapterException("Error when contacting Game Service", response.url, response.status_code,
                                   error_message)

    @classmethod
    def getGames(cls) -> List[GameInfoResponseBody]:
        url = f'{gameHost}/games'
        response = requests.get(url)
        match response.status_code:
            case HTTPStatus.OK:
                gameInfoResponseBodies = [GameInfoResponseBody(**game) for game in response.json()]
                return gameInfoResponseBodies
            case _:
                cls.__handleUnknownError(response)

    @classmethod
    def getGame(cls, gameId: str) -> Optional[GameInfoResponseBody]:
        games = cls.getGames()
        for game in games:
            if game.gameId == gameId:
                return game
        return None

    @classmethod
    def registerPlayer(cls, name: str, email: str) -> Optional[PlayerRegistrationResponseBody]:
        url = f'{gameHost}/players'
        response = requests.post(url,
                                 json=PlayerRegistrationRequestBody(name=name,
                                                                    email=email).model_dump())
        match response.status_code:
            case HTTPStatus.CREATED:
                playerRegistrationInfo = PlayerRegistrationResponseBody(**response.json())
                cls._logger.info(playerRegistrationInfo)
                return playerRegistrationInfo
            case HTTPStatus.BAD_REQUEST:
                cls._logger.warning(f'Failed to register player: Player with same name already exists')
                return None
            case _:
                cls.__handleUnknownError(response)

    @classmethod
    def getPlayer(cls) -> Optional[PlayerRegistrationResponseBody]:
        queryParams = {
            'name': Config.PLAYER_NAME,
            'mail': Config.PLAYER_EMAIL
        }
        response = requests.get(f'{gameHost}/players', params=queryParams)
        match response.status_code:
            case HTTPStatus.OK:
                playerRegistrationInfo = PlayerRegistrationResponseBody(**response.json())
                cls._logger.info(playerRegistrationInfo)
                return playerRegistrationInfo
            case HTTPStatus.NOT_FOUND:
                cls._logger.warning(
                    f'Failed to get player: Player with name {Config.PLAYER_NAME} and email {Config.PLAYER_EMAIL} not found')
                return None
            case _:
                cls.__handleUnknownError(response)

    @classmethod
    def joinGame(cls, gameId: str, playerId: str) -> bool:
        response = requests.put(f'{gameHost}/games/{gameId}/players/{playerId}')
        match response.status_code:
            case HTTPStatus.OK:
                cls._logger.info(f'Player {playerId} joined game {gameId} successfully')
                return True
            case HTTPStatus.BAD_REQUEST:
                cls._logger.warning(f'Failed to join game: It is either full or has already started.')
                return False
            case HTTPStatus.NOT_FOUND:
                cls._logger.warning(f'Failed to join game: Player {playerId} or Game {gameId} not found.')
                return False
            case _:
                cls.__handleUnknownError(response)

    @classmethod
    def createGame(cls, maxPlayers: int, maxRounds: int) -> Optional[GameCreatedResponseBody]:
        response = requests.post(f'{gameHost}/games',
                                 json=CreateGameRequestBody(maxPlayers=maxPlayers, maxRounds=maxRounds).model_dump())
        match response.status_code:
            case HTTPStatus.CREATED:
                gameInfo = GameCreatedResponseBody(**response.json())
                cls._logger.info(f'Game {gameInfo.gameId} created successfully')
                return gameInfo
            case HTTPStatus.BAD_REQUEST:
                cls._logger.warning(
                    f'Failed to create game: An active game already exists. A game is considered active when '
                    f'its status is either "CREATED" or "RUNNING". Active games have to be closed in order to '
                    f'create a new one.')
                return None
            case _:
                cls.__handleUnknownError(response)

    @classmethod
    def startGame(cls, gameId: str) -> bool:
        response = requests.post(f'{gameHost}/games/{gameId}/gameCommands/start')
        match response.status_code:
            case HTTPStatus.CREATED:
                cls._logger.info(f'Game {gameId} started successfully')
                return True
            case HTTPStatus.BAD_REQUEST:
                cls._logger.warning(
                    f'Failed to start game: Game {gameId} is not in a state it could be started from. It is either running or closed')
                return False
            case HTTPStatus.NOT_FOUND:
                cls._logger.warning(f'Failed to start game: Game {gameId} not found.')
                return False
            case _:
                cls.__handleUnknownError(response)

    @classmethod
    def endGame(cls, gameId: str):
        response = requests.post(f'{gameHost}/games/{gameId}/gameCommands/end')
        match response.status_code:
            case HTTPStatus.CREATED:
                cls._logger.info(f'Game {gameId} ended successfully')
                return True
            case HTTPStatus.BAD_REQUEST:
                cls._logger.warning(
                    f'Failed to end game: Game {gameId} is not in a state it could be ended from. It is either running or '
                    f'closed')
                return False
            case HTTPStatus.NOT_FOUND:
                cls._logger.warning(f'Failed to end game: Game {gameId} not found.')
                return False
            case _:
                cls.__handleUnknownError(response)

    @classmethod
    def endAllActiveGames(cls):
        for game in GameServiceRestAdapter.getGames():
            if game.gameStatus == GameStatus.CREATED.value:
                GameServiceRestAdapter.startGame(game.gameId)
                time.sleep(3)
            GameServiceRestAdapter.endGame(game.gameId)

    @classmethod
    def patchRoundTime(cls, gameId: str, durationInMillis: int) -> bool:
        response = requests.patch(f'{gameHost}/games/{gameId}/duration',
                                  json=PatchRoundTimeRequestBody(duration=durationInMillis).model_dump())
        match response.status_code:
            case HTTPStatus.OK:
                cls._logger.info(f'Round time of game {gameId} reduced successfully')
                return True
            case HTTPStatus.NOT_FOUND:
                cls._logger.warning(f'Failed to reduce round time: Game {gameId} not found.')
                return False
            case _:
                cls.__handleUnknownError(response)

    @classmethod
    def postCommand(cls, command: Command):
        response = requests.post(f'{gameHost}/games/{command.gameId}/commands',
                                 json=command.model_dump())
        match response.status_code:
            case HTTPStatus.CREATED:
                cls._logger.info(f'Command {command} successfully posted')
                return True
            case HTTPStatus.BAD_REQUEST:
                cls._logger.warning(
                    f"Syntax invalid, Game not ready (has't started yet or already finished) or command invalid. That "
                    f"may happen if the player is trying to send commands for robots that do not belong to him.")
                return False
            case HTTPStatus.NOT_FOUND:
                cls._logger.warning(f'Failed to post command {command}: Game {command.gameId} not found.')
                return False
            case _:
                cls.__handleUnknownError(response)
