from typing import Optional, List

from pydantic import BaseModel


class GameInfoResponseBody(BaseModel):
    gameId: str
    gameStatus: str
    maxPlayers: int
    maxRounds: int
    roundLengthInMillis: int
    currentRoundNumber: Optional[int]
    participatingPlayers: List[str]

    class Config:
        extra = "ignore"
