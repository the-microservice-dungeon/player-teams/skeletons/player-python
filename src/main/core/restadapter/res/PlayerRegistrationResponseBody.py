from pydantic import BaseModel

from src.main.Config import Config


class PlayerRegistrationResponseBody(BaseModel):
    playerId: str
    name: str
    email: str
    playerExchange: str = f'player-{Config.PLAYER_NAME}'
    playerQueue: str = f'player-{Config.PLAYER_NAME}'
