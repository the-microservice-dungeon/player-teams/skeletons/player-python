from pydantic import BaseModel


class GameCreatedResponseBody(BaseModel):
    gameId: str
