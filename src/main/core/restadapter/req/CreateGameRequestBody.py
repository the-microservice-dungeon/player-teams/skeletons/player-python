from pydantic import BaseModel


class CreateGameRequestBody(BaseModel):
    maxPlayers: int
    maxRounds: int