from pydantic import BaseModel


class PatchRoundTimeRequestBody(BaseModel):
    duration: int
