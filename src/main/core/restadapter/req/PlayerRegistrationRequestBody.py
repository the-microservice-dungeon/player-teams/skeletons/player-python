from pydantic import BaseModel


class PlayerRegistrationRequestBody(BaseModel):
    name: str
    email: str
