class RestAdapterException(Exception):
    def __init__(self, message: str, url: str, status_code: int, error_message: str = None):
        self.message = message
        self.url = url
        self.status_code = status_code
        self.error_message = error_message
        super().__init__(self.__str__())

    def __str__(self):
        return (f"{self.message} - URL: {self.url}, Status Code: {self.status_code}, "
                f"Error Message: {self.error_message or 'None'}")
