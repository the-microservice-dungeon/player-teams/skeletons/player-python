import logging
import os
from typing import get_type_hints, Union

from dotenv import load_dotenv

load_dotenv()


class AppConfigError(Exception):
    def __init__(self, message: str):
        super().__init__(message)


def _parse_bool(val: Union[str, bool]) -> bool:  # pylint: disable=E1136
    return val if type(val) == bool else val.lower() in ['true', 'yes', '1']


class AppConfig:
    DEV: bool = False
    RABBITMQ_HOST: str = '127.0.0.1'
    RABBITMQ_PORT: int = 5672
    RABBITMQ_USERNAME: str = 'admin'
    RABBITMQ_PASSWORD: str = 'admin'
    GAME_HOST: str = 'http://127.0.0.1'
    GAME_PORT: int = 8080
    PLAYER_NAME: str = 'player-skeleton-python'
    PLAYER_EMAIL: str = 'player-skeleton-python@example.com'
    LOG_LEVEL: str = 'INFO'
    DATA_ENDPOINT_PORT: int = 8090

    """
    Map environment variables to class fields according to these rules:
      - Field won't be parsed unless it has a type annotation
      - Field will be skipped if not in all caps
      - Class field and environment variable name are the same
    """

    def __init__(self, env):
        for field in self.__annotations__:
            if not field.isupper():
                continue

            # Raise AppConfigError if required field not supplied
            default_value = getattr(self, field, None)
            env_value = env.get(field)

            if env_value is None:
                if default_value is not None:
                    logging.warning(f"Environment variable {field} not set, using default: {default_value}")
                else:
                    raise AppConfigError(f'The {field} field is required')

            # Cast env var value to expected type and raise AppConfigError on failure
            try:
                var_type = get_type_hints(AppConfig)[field]
                if var_type == bool:
                    value = _parse_bool(env.get(field, default_value))
                else:
                    value = var_type(env.get(field, default_value))

                self.__setattr__(field, value)
            except ValueError:
                raise AppConfigError('Unable to cast value of "{}" to type "{}" for "{}" field'.format(
                    env[field],
                    var_type,
                    field
                )
                )

    def __repr__(self):
        return str(self.__dict__)


Config = AppConfig(os.environ)
